﻿using UnityEngine;
using System.Collections;

public class MonaLisaText : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TextMesh textObject = GameObject.Find("monaLisaText").GetComponent<TextMesh>();
		textObject.text = "";	
		TextMesh textObject1 = GameObject.Find("1789").GetComponent<TextMesh>();
		textObject1.text = "";	
		TextMesh textObject2 = GameObject.Find("1868").GetComponent<TextMesh>();
		textObject2.text = "";
		TextMesh textObject3 = GameObject.Find("2015").GetComponent<TextMesh>();
		textObject3.text = "";
		TextMesh textObject4 = GameObject.Find("mapText1").GetComponent<TextMesh>();
		textObject4.text = "";
		TextMesh textObject5 = GameObject.Find("mapText2").GetComponent<TextMesh>();
		textObject5.text = "";

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void displayMonaLisaText() {
		TextMesh textObject = GameObject.Find("monaLisaText").GetComponent<TextMesh>();
		textObject.text = "Su nombre se debe a ser el segundo hijo varon,\n derrotado en la batalla de Waterloo y \nEmperador de Francia";	
		StartCoroutine (timeOutText (textObject));
	}

	public void displayNapoleonText() {
		TextMesh textObject1 = GameObject.Find("1789").GetComponent<TextMesh>();
		textObject1.text = "MDCCLXXXIX";	
		TextMesh textObject2 = GameObject.Find("1868").GetComponent<TextMesh>();
		textObject2.text = "MDCCCLXVIII";
		TextMesh textObject3 = GameObject.Find("2015").GetComponent<TextMesh>();
		textObject3.text = "MMXV";
		StartCoroutine (timeOutText (textObject1));
		StartCoroutine (timeOutText (textObject2));
		StartCoroutine (timeOutText (textObject3));
	}

	public void displayMapText() {
		TextMesh textObject1 = GameObject.Find("mapText1").GetComponent<TextMesh>();
		textObject1.text = "El territorio de Francia, y su parte\nmetropolitana o continental,5 se ubica\nen Europa Occidental.\nCon capital en París, se extiende\n sobre una superficie total de 675 417 km²\n y cuenta con una población de 66 millones\nde habitantes\n";	
		TextMesh textObject2 = GameObject.Find("mapText2").GetComponent<TextMesh>();
		textObject2.text = "Fronteras naturales: el mar del Norte,\nel canal de la Mancha, el océano Atlántico;\nlos Pirineos (frontera con España y Andorra);\nel mar Mediterráneo (golfo de León, Costa Azul);\nlos Alpes; los montes Jura; el río Rin\n";
		StartCoroutine (timeOutTextTen (textObject1));
		StartCoroutine (timeOutTextTen (textObject2));

	}

	IEnumerator timeOutText(TextMesh textObject)
	{
		yield return new WaitForSeconds(5.0f);
		textObject.text = "";
	}

	IEnumerator timeOutTextTen(TextMesh textObject)
	{
		yield return new WaitForSeconds(10.0f);
		textObject.text = "";
	}

}
