﻿using UnityEngine;
using System.Collections;

public class Egipto : MonoBehaviour {

	public float wait = 7.0f;
	// Use this for initialization
	void Start () {
		TextMesh textEsfinge = GameObject.Find("esfingeText").GetComponent<TextMesh>();
		textEsfinge.text = "";
		TextMesh textEsfinges = GameObject.Find("text piramides").GetComponent<TextMesh>();
		textEsfinges.text = "";
		TextMesh textEsfingei = GameObject.Find("textNefertiti").GetComponent<TextMesh>();
		textEsfingei.text = "";
		TextMesh textFaraon = GameObject.Find("TextFaraon").GetComponent<TextMesh>();
		textFaraon.text = "";
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	public void displayTextEsfinge() {
		TextMesh textObject = GameObject.Find("esfingeText").GetComponent<TextMesh>();
		textObject.text = "La Gran Esfinge de Guiza\n es una monumental escultura que se\n encuentra en la ribera occidental del\n río Nilo con la gran piramide\n a sus espaldas, en la ciudad\n de Guiza, unos veinte kilómetros\n al sudoeste del centro de\n El Cairo" ;
		StartCoroutine (timeOutText (textObject));
	}
	public void displayTextFaraon() {
		TextMesh textObject = GameObject.Find("TextFaraon").GetComponent<TextMesh>();
		textObject.text = "El Valle de los Reyes\n es una necrópolis del\n antiguo Egipto, en las\n cercanías de Luxor,\n donde se encuentran\n las tumbas de la\n mayoría de faraones\n del Imperio Nuevo";
		StartCoroutine (timeOutText (textObject));
	}
	public void displayTextPiramides() {
		TextMesh textObject = GameObject.Find("text piramides").GetComponent<TextMesh>();
		textObject.text = "  Las Pirámides:\n Son los vestigios más sorprendentes \n dejados por esta cultura las\n mas representantes son Guiza, \n Jafra y Menkaura";
		StartCoroutine (timeOutText (textObject));

	}

	public void displayTextNefertiti() {
		TextMesh textObject = GameObject.Find("textNefertiti").GetComponent<TextMesh>();
		textObject.text = "La tumba KV55\n esta en el Valle de los Reyes \n es de la dinastía XVIII,\n siendo un lugar arqueológico\n problemático pues parece\n haber sido utilizado para\n varios enterramientos";
		StartCoroutine (timeOutText (textObject));

	}




	IEnumerator timeOutText(TextMesh textObject)
	{
		yield return new WaitForSeconds(wait);
		textObject.text = "";
	}
}
