﻿using UnityEngine;
using System.Collections;

public class occscript : MonoBehaviour {
	// Use this for initialization
	public AudioClip mySound;
	public AudioSource mySource;
	//public string description;
	void Start () { 
		
		
		TextMesh textObject = GameObject.Find("occol").GetComponent<TextMesh>();
		textObject.text = "";
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	
	public void modifyText() {
		TextMesh textObject = GameObject.Find("occol").GetComponent<TextMesh>();
		
		// in method, assign the clip to the audioSource
		mySource.clip = mySound;
		// AudioSource.Play();
		mySource.Play();
		
		
		textObject.text = "La Cordillera Occidental\n tiene como singular característica\n el vulcanismo de sus cumbres,\n y hace de frontera natural con Chile.";
		
		StartCoroutine (timeOutText (textObject));
		
	}
	
	IEnumerator timeOutText(TextMesh textObject)
	{
		yield return new WaitForSeconds(10.0f);
		textObject.text = "";
	}
	
	
	public void upText() {
		TextMesh textObject = GameObject.Find("occol").GetComponent<TextMesh>();
		textObject.text = "up";
	}
	
	public void downText() {
		TextMesh textObject = GameObject.Find("occol").GetComponent<TextMesh>();
		textObject.text = "down";
	}
}
