﻿using UnityEngine;
using System.Collections;

public class DissapearText : MonoBehaviour {
	// Use this for initialization
	public AudioClip mySound;
	public AudioSource mySource;
	//public string description;
	void Start () { 


		TextMesh textObject = GameObject.Find("TextUl").GetComponent<TextMesh>();
		textObject.text = "";

	}
	
	// Update is called once per frame
	void Update () {


	}

	public void modifyText() {
		TextMesh textObject = GameObject.Find("TextUl").GetComponent<TextMesh>();

		// in method, assign the clip to the audioSource
		mySource.clip = mySound;
		// AudioSource.Play();
		mySource.Play();


		textObject.text = "El Uluṟu también llamado Ayers Rock\n es una formación rocosa\n compuesta por arenisca\n esta en el centro de Australia";
		StartCoroutine (timeOutText (textObject));
	
	}

	IEnumerator timeOutText(TextMesh textObject)
	{
		yield return new WaitForSeconds(5.0f);
		textObject.text = "";
	}

	public void upText() {
		TextMesh textObject = GameObject.Find("TextUl").GetComponent<TextMesh>();
		textObject.text = "up";
	}

	public void downText() {
		TextMesh textObject = GameObject.Find("TextUl").GetComponent<TextMesh>();
		textObject.text = "down";
	}
}
