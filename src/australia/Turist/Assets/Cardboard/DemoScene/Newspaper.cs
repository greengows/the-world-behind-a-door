// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using System.Collections;
using System.Threading;
using System;

[RequireComponent(typeof(Collider))]
public class Newspaper : MonoBehaviour {
	

	
	void Start() {
		
	}
	
	public void SetGazedAt(bool gazedAt) {
		//GetComponent<Renderer>().material.color = gazedAt ? Color.green : Color.red;
		
	}
	
	public void Reset() {
	}
	
	public void ToggleVRMode() {
		Cardboard.SDK.VRModeEnabled = !Cardboard.SDK.VRModeEnabled;
	}
	//Vector3 direction = textObject.transform.localPosition;
	//textObject.transform.localPosition = direction;
	
	public void showMesssage(){
		TextMesh textObject = GameObject.Find ("MessageDoor").GetComponent<TextMesh> ();
		textObject.text = "Al parecer es un\nanuncio para reclutar\ngente en la armada...";
		
		StartCoroutine (GreenYellowRed (textObject));
	}
	
	IEnumerator GreenYellowRed(TextMesh textObject)
	{
		yield return new WaitForSeconds(5.0f);
		textObject.text = "";
	}
	
}
