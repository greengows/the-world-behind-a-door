﻿using UnityEngine;
using System.Collections;

public class teplacod : MonoBehaviour {
	// Use this for initialization
	public AudioClip mySound;
	public AudioSource mySource;
	//public string description;
	void Start () { 
		
		
		TextMesh textObject = GameObject.Find("Tepla").GetComponent<TextMesh>();
		textObject.text = "";
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	
	public void modifyText() {
		TextMesh textObject = GameObject.Find("Tepla").GetComponent<TextMesh>();
		
		// in method, assign the clip to the audioSource
		mySource.clip = mySound;
		// AudioSource.Play();
		mySource.Play();
		
		
		textObject.text = "La placa Indoaustraliana\n es una placa tectónica\n que se extiende desde la frontera\n de la India con China y Nepal,\n por ello movimientos en la india\n pueden tener consecuencias en australia";
		StartCoroutine (timeOutText (textObject));
	
	}

	IEnumerator timeOutText(TextMesh textObject)
	{
		yield return new WaitForSeconds(10.0f);
		textObject.text = "";
	}
	
	public void upText() {
		TextMesh textObject = GameObject.Find("Tepla").GetComponent<TextMesh>();
		textObject.text = "up";
	}
	
	public void downText() {
		TextMesh textObject = GameObject.Find("Tepla").GetComponent<TextMesh>();
		textObject.text = "down";
	}
}
