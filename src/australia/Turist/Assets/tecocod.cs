﻿using UnityEngine;
using System.Collections;

public class tecocod : MonoBehaviour {
	// Use this for initialization
	public AudioClip mySound;
	public AudioSource mySource;
	//public string description;
	void Start () { 
		
		
		TextMesh textObject = GameObject.Find("Teco").GetComponent<TextMesh>();
		textObject.text = "";
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	
	public void modifyText() {
		TextMesh textObject = GameObject.Find("Teco").GetComponent<TextMesh>();
		
		// in method, assign the clip to the audioSource
		mySource.clip = mySound;
		// AudioSource.Play();
		mySource.Play();
		
		
		textObject.text = "La Gran Barrera de Coral australiana,\n es el mayor arrecife de coral del mundo.\n El arrecife está situado en el mar del Coral,\n frente a la costa de Queensland\n al noreste de Australia.";
		StartCoroutine (timeOutText (textObject));

	}

	IEnumerator timeOutText(TextMesh textObject)
	{
		yield return new WaitForSeconds(10.0f);
		textObject.text = "";
	}
	
	public void upText() {
		TextMesh textObject = GameObject.Find("Teco").GetComponent<TextMesh>();
		textObject.text = "up";
	}
	
	public void downText() {
		TextMesh textObject = GameObject.Find("Teco").GetComponent<TextMesh>();
		textObject.text = "down";
	}
}
