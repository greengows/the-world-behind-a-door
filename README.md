### THE WORLD BEHIND A DOOR ###

![greengows.jpg](https://bitbucket.org/repo/a447r4/images/2299493577-greengows.jpg)

Nombre Equipo:  
Greengows

# Bienvenidos! #

## Descripción ##

**The world behind a door** es un juego de realidad virtual orientado solamente a la enseñanza y educación de Geografía a personas que están comprendidas entre los quince a dieciocho años de edad, esto no es excluyente ya que en la feria del libro realizada en la ciudad de La Paz - Bolivia desde fecha 6 de Agosto hasta 15 de Agosto se pudo presenciar como el juego era requerido por personas de toda edad, sorprendidas ante la característica principal del juego como es la realidad virtual.

El objetivo del juego es resolver los acertijos relacionados con geografía y pasar a la siguiente habitación, dando la sensación de que el usuario está en el centro de cada habitación, pudiendo de esta manera observar hacia los lados de manera libre.

Es necesario el uso de una cardboard mas audífonos para una experiencia completa en este campo.

Para mayor información acerca de como el juego funciona existe el siguiente video acerca del juego:

http://youtu.be/L8uJ99UddBQ

Y el juego como tal mas sus instrucciones de uso se los puede descargar de la siguiente dirección

https://www.dropbox.com/sh/jglh1vk59jkiapb/AAAonkC5ECip9Ga3fyyw22hBa?dl=0

A continuación se presenta imágenes de las distintas habitaciones con las que se cuenta a través del juego, es cuestión de tiempo y esfuerzo para aumentar más de ellas representando distintos países, ya que la base del desarrollo ya está realizada.

## Habitación de Estados Unidos ##

![usa.png](https://bitbucket.org/repo/a447r4/images/2534833972-usa.png)

## Habitación de Egipto ##

![egipto.png](https://bitbucket.org/repo/a447r4/images/2952849478-egipto.png)

## Habitación de Bolivia ##

![bolivia.png](https://bitbucket.org/repo/a447r4/images/1596745796-bolivia.png)

## Habitación de Francia ##

![france.png](https://bitbucket.org/repo/a447r4/images/3546486910-france.png)

## Habitación de Australia ##

![australia.png](https://bitbucket.org/repo/a447r4/images/3532333191-australia.png)


Disfruta del juego!!!

Por favor, cualquier duda o si estás interesado en poder trabajar juntos, no dudes en contactarnos

Contactos:

greengows@gmail.com